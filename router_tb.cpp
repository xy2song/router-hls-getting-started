#include "/opt/Xilinx/Vivado/2018.3/include/gmp.h"  //cosim workaround for know Vivado HLS bug
#include <stdio.h>
#include "top.h"
#include "ap_int.h"

//define NOC_PKT struct operators
bool operator==(const NOC_PKT& lhs, const NOC_PKT& rhs) {
    return (lhs.dst_router_addr == rhs.dst_router_addr) && (lhs.payload == rhs.payload);
}

bool operator!=(const NOC_PKT& lhs, const NOC_PKT& rhs) {
    return (lhs.dst_router_addr != rhs.dst_router_addr) || (lhs.payload != rhs.payload);
}

int main() {
    ap_uint<2> dut_router_addr = 1;
    NOC_PKT left_pkt;
    NOC_PKT up_pkt;
    NOC_PKT down_pkt;
    bool left_vld;
    bool up_vld;
    bool down_vld;
    NOC_PKT west_out;
    NOC_PKT east_out;
    NOC_PKT north_out;
    NOC_PKT south_out;
    bool west_out_vld;
    bool east_out_vld;
    bool north_out_vld;
    bool south_out_vld;

    unsigned int err_cnt = 0;

    ap_uint<36> index_and_data;
    ap_uint<36> left_pkt_index_and_data;
    ap_uint<36> up_pkt_index_and_data;
    ap_uint<36> down_pkt_index_and_data;
    ap_uint<2> left_addr;
    ap_uint<2> up_addr;
    ap_uint<2> down_addr;

    static ap_uint<1> east_prev_win = DOWN; // arbiter's previously grant.
    static ap_uint<1> north_prev_win = DOWN;
    static ap_uint<1> south_prev_win = DOWN;
    
    // no competition - everyone served
    /*         
                ______________
     east req  |  1 |  0 |  0*|       * =  previous win
               |____|____|____|
                left  up  down
                ______________
     north req |  0 |  0 |  1*| 
               |____|____|____|
                left  up  down
                ______________
     south req |  0 |  1 |  0*| 
               |____|____|____|
                left  up  down
    */
    left_pkt.dst_router_addr = 0b01; //going east
    up_pkt.dst_router_addr   = 0b11; //going south
    down_pkt.dst_router_addr = 0b00; //going north
    left_pkt.payload = 0x00000000;
    up_pkt.payload   = 0x00000001;
    down_pkt.payload = 0x00000002;

    left_vld = 1;
    up_vld   = 1;
    down_vld = 1;

    router(dut_router_addr, 
           left_pkt, up_pkt, down_pkt, 
           left_vld, up_vld, down_vld, 
           &(east_prev_win), &(north_prev_win), &(south_prev_win),
           &west_out, &east_out, &north_out, &south_out, 
           &west_out_vld, &east_out_vld, &north_out_vld, &south_out_vld);

    if ( east_out != left_pkt || north_out != down_pkt || south_out != up_pkt
      || west_out_vld != 0 || east_out_vld != 1 || north_out_vld != 1 || south_out_vld != 1){
        printf("ERROR: test 0 fail. \n");
        err_cnt++;
    } else {
        printf("test 0 pass. \n");
    }

    // full competition: up wins, left and down deflected
    /*         
                ______________
     east req  |  1 |  1 |  1 |       * = previous win
               |____|____|____|
                left  up  down*
                ______________
     north req |  0 |  0 |  0 | 
               |____|____|____|
                left  up  down*
                ______________
     south req |  0 |  0 |  0 | 
               |____|____|____|
                left  up* down
    */
    left_pkt.dst_router_addr = 0b01;
    up_pkt.dst_router_addr   = 0b01;
    down_pkt.dst_router_addr = 0b01;
    left_pkt.payload = 0x00000010;
    up_pkt.payload   = 0x00000011;
    down_pkt.payload = 0x00000012;
    
    left_vld = 1;
    up_vld   = 1;
    down_vld = 1;

    router(dut_router_addr, 
           left_pkt, up_pkt, down_pkt, 
           left_vld, up_vld, down_vld, 
           &(east_prev_win), &(north_prev_win), &(south_prev_win),
           &west_out, &east_out, &north_out, &south_out, 
           &west_out_vld, &east_out_vld, &north_out_vld, &south_out_vld);

    if ( east_out != up_pkt || west_out != left_pkt || south_out != down_pkt
      || west_out_vld != 1 || east_out_vld != 1 || north_out_vld != 0 || south_out_vld != 1){
        printf("ERROR: test 1 fail. \n");
        err_cnt++;
    } else {
        printf("test 1 pass. \n");
    }


    // up down competition: down wins, up deflected
    /*         
                ______________
     east req  |  0 |  1 |  1 |       * = previous win
               |____|____|____|
                left  up* down 
                ______________
     north req |  0 |  0 |  0 | 
               |____|____|____|
                left  up  down*
                ______________
     south req |  0 |  0 |  0 | 
               |____|____|____|
                left  up* down
    */
    left_pkt.dst_router_addr = 0b00;
    up_pkt.dst_router_addr   = 0b01;
    down_pkt.dst_router_addr = 0b01;
    left_pkt.payload = 0x00000020;
    up_pkt.payload   = 0x00000021;
    down_pkt.payload = 0x00000022;

    left_vld = 0;
    up_vld   = 1;
    down_vld = 1;

    router(dut_router_addr, 
           left_pkt, up_pkt, down_pkt, 
           left_vld, up_vld, down_vld, 
           &(east_prev_win), &(north_prev_win), &(south_prev_win),
           &west_out, &east_out, &north_out, &south_out, 
           &west_out_vld, &east_out_vld, &north_out_vld, &south_out_vld);

    if ( east_out != down_pkt || north_out != up_pkt
      || west_out_vld != 0 || east_out_vld != 1 || north_out_vld != 1 || south_out_vld != 0){
        printf("ERROR: test 2 fail. \n");
        err_cnt++;
    } else {
        printf("test 2 pass. \n");
    }

    // left down competition: down wins, left deflected
    /*         
                ______________
     east req  |  1 |  0 |  1 |       * = previous win
               |____|____|____|
                left  up  down*
                ______________
     north req |  0 |  0 |  0 | 
               |____|____|____|
                left  up  down*
                ______________
     south req |  0 |  0 |  0 | 
               |____|____|____|
                left  up* down
    */

    left_pkt.dst_router_addr = 0b01;
    up_pkt.dst_router_addr   = 0b00;
    down_pkt.dst_router_addr = 0b01;
    left_pkt.payload = 0x00000030;
    up_pkt.payload   = 0x00000031;
    down_pkt.payload = 0x00000032;

    left_vld = 1;
    up_vld   = 0;
    down_vld = 1;

    router(dut_router_addr, 
           left_pkt, up_pkt, down_pkt, 
           left_vld, up_vld, down_vld, 
           &(east_prev_win), &(north_prev_win), &(south_prev_win),
           &west_out, &east_out, &north_out, &south_out, 
           &west_out_vld, &east_out_vld, &north_out_vld, &south_out_vld);

    if ( west_out != left_pkt || east_out != down_pkt
      || west_out_vld != 1 || east_out_vld != 1 || north_out_vld != 0 || south_out_vld != 0){
        printf("ERROR: test 3 fail. \n");
        err_cnt++;
    } else {
        printf("test 3 pass. \n");
    }

    // all competition: up wins, left and down deflected
    /*         
                ______________
     east req  |  0 |  0 |  0 |       * = previous win
               |____|____|____|
                left  up  down*
                ______________
     north req |  1 |  1 |  1 | 
               |____|____|____|
                left  up  down*
                ______________
     south req |  0 |  0 |  0 | 
               |____|____|____|
                left  up* down
    */
    left_pkt.dst_router_addr = 0b00;
    up_pkt.dst_router_addr   = 0b00;
    down_pkt.dst_router_addr = 0b00;
    left_pkt.payload = 0x00000040;
    up_pkt.payload   = 0x00000041;
    down_pkt.payload = 0x00000042;

    left_vld = 1;
    up_vld   = 1;
    down_vld = 1;

    router(dut_router_addr, 
           left_pkt, up_pkt, down_pkt, 
           left_vld, up_vld, down_vld, 
           &(east_prev_win), &(north_prev_win), &(south_prev_win),
           &west_out, &east_out, &north_out, &south_out, 
           &west_out_vld, &east_out_vld, &north_out_vld, &south_out_vld);

    if ( west_out != left_pkt || north_out != up_pkt || south_out != down_pkt
      || west_out_vld != 1 || east_out_vld != 0 || north_out_vld != 1 || south_out_vld != 1){
        printf("ERROR: test 4 fail. \n");
        err_cnt++;
    } else {
        printf("test 4 pass. \n");
    }

    // all competition: down wins, left and up deflected
    /*         
                ______________
     east req  |  0 |  0 |  0 |       * = previous win
               |____|____|____|
                left  up  down*
                ______________
     north req |  0 |  0 |  0 | 
               |____|____|____|
                left  up* down 
                ______________
     south req |  1 |  1 |  1 | 
               |____|____|____|
                left  up* down
    */

    left_pkt.dst_router_addr = 0b11;
    up_pkt.dst_router_addr   = 0b11;
    down_pkt.dst_router_addr = 0b11;
    left_pkt.payload = 0x00000050;
    up_pkt.payload   = 0x00000051;
    down_pkt.payload = 0x00000052;

    left_vld = 1;
    up_vld   = 1;
    down_vld = 1;

    router(dut_router_addr, 
           left_pkt, up_pkt, down_pkt, 
           left_vld, up_vld, down_vld, 
           &(east_prev_win), &(north_prev_win), &(south_prev_win),
           &west_out, &east_out, &north_out, &south_out, 
           &west_out_vld, &east_out_vld, &north_out_vld, &south_out_vld);

    if ( west_out != left_pkt || north_out != up_pkt || south_out != down_pkt
      || west_out_vld != 1 || east_out_vld != 0 || north_out_vld != 1 || south_out_vld != 1){
        printf("ERROR: test 5 fail. \n");
        err_cnt++;
    } else {
        printf("test 5 pass. \n");
    }

    // up down competition: down wins, up deflected
    /*         
                ______________
     east req  |  0 |  0 |  0 |       * = previous win
               |____|____|____|
                left  up  down*
                ______________
     north req |  0 |  1 |  1 | 
               |____|____|____|
                left  up* down 
                ______________
     south req |  0 |  0 |  0 | 
               |____|____|____|
                left  up  down*
    */

    left_pkt.dst_router_addr = 0b00;
    up_pkt.dst_router_addr   = 0b00;
    down_pkt.dst_router_addr = 0b00;
    left_pkt.payload = 0x00000060;
    up_pkt.payload   = 0x00000061;
    down_pkt.payload = 0x00000062;

    left_vld = 0;
    up_vld   = 1;
    down_vld = 1;

    router(dut_router_addr, 
           left_pkt, up_pkt, down_pkt, 
           left_vld, up_vld, down_vld, 
           &(east_prev_win), &(north_prev_win), &(south_prev_win),
           &west_out, &east_out, &north_out, &south_out, 
           &west_out_vld, &east_out_vld, &north_out_vld, &south_out_vld);

    if ( north_out != down_pkt || south_out != up_pkt
      || west_out_vld != 0 || east_out_vld != 0 || north_out_vld != 1 || south_out_vld != 1){
        printf("ERROR: test 6 fail. \n");
        err_cnt++;
    } else {
        printf("test 6 pass. \n");
    }

    // left up competition: up wins, left deflected
    /*         
                ______________
     east req  |  0 |  0 |  0 |       * = previous win
               |____|____|____|
                left  up  down*
                ______________
     north req |  0 |  0 |  0 | 
               |____|____|____|
                left  up  down*
                ______________
     south req |  1 |  1 |  0 | 
               |____|____|____|
                left  up  down*
    */

    left_pkt.dst_router_addr = 0b11;
    up_pkt.dst_router_addr   = 0b11;
    down_pkt.dst_router_addr = 0b00;
    left_pkt.payload = 0x00000070;
    up_pkt.payload   = 0x00000071;
    down_pkt.payload = 0x00000072;

    left_vld = 1;
    up_vld   = 1;
    down_vld = 0;

    router(dut_router_addr, 
           left_pkt, up_pkt, down_pkt, 
           left_vld, up_vld, down_vld, 
           &(east_prev_win), &(north_prev_win), &(south_prev_win),
           &west_out, &east_out, &north_out, &south_out, 
           &west_out_vld, &east_out_vld, &north_out_vld, &south_out_vld);

    if ( west_out != left_pkt || south_out != up_pkt
      || west_out_vld != 1 || east_out_vld != 0 || north_out_vld != 0 || south_out_vld != 1){
        printf("ERROR: test 7 fail. \n");
        err_cnt++;
    } else {
        printf("test 7 pass. \n");
    }

    // full competition: down wins, up deflected
    /*         
                ______________
     east req  |  0 |  0 |  0 |       * = previous win
               |____|____|____|
                left  up  down*
                ______________
     north req |  0 |  0 |  0 | 
               |____|____|____|
                left  up  down*
                ______________
     south req |  0 |  1 |  1 | 
               |____|____|____|
                left  up* down 
    */

    left_pkt.dst_router_addr = 0b00;
    up_pkt.dst_router_addr   = 0b11;
    down_pkt.dst_router_addr = 0b11;
    left_pkt.payload = 0x00000080;
    up_pkt.payload   = 0x00000081;
    down_pkt.payload = 0x00000082;

    left_vld = 0;
    up_vld   = 1;
    down_vld = 1;

    router(dut_router_addr, 
           left_pkt, up_pkt, down_pkt, 
           left_vld, up_vld, down_vld, 
           &(east_prev_win), &(north_prev_win), &(south_prev_win),
           &west_out, &east_out, &north_out, &south_out, 
           &west_out_vld, &east_out_vld, &north_out_vld, &south_out_vld);

    if ( north_out != up_pkt || south_out != down_pkt
      || west_out_vld != 0 || east_out_vld != 0 || north_out_vld != 1 || south_out_vld != 1){
        printf("ERROR: test 8 fail. \n");
        err_cnt++;
    } else {
        printf("test 8 pass. \n");
    }

    if (err_cnt) {
       printf("!!! TEST FAILED - %d errors detected !!!\n", err_cnt);
    } else
       printf("*** Test Passed ***\n");

    return err_cnt;
}
