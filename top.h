#include "/opt/Xilinx/Vivado/2018.3/include/gmp.h" //cosim workaround for know Vivado HLS bug
#include "ap_int.h"


#define NUM_BITS_ROUTER_ADDR  2
#define NUM_BITS_PAYLOAD      32
#define NUM_BITS_NOC_PKT      NUM_BITS_VEC_INDEX+NUM_BITS_ROUTER_ADDR+NUM_BITS_PAYLOAD

////router
//input directions
#define UP   0
#define DOWN 1
#define LEFT 2
#define DONT_CARE   0
//output directions
#define NO_REQ 0
#define EAST  1
#define NORTH 2
#define SOUTH 3

typedef float DATA_T;  // Data type used for math

struct NOC_PKT {
    ap_uint<NUM_BITS_ROUTER_ADDR> dst_router_addr;
    DATA_T payload;
};

void router (ap_uint<NUM_BITS_ROUTER_ADDR> router_addr,
             NOC_PKT left_in, NOC_PKT up_in, NOC_PKT down_in, 
             bool left_in_vld, bool up_in_vld, bool down_in_vld, 
             ap_uint<1>* east_prev_win, ap_uint<1>* north_prev_win, ap_uint<1>* south_prev_win,
             NOC_PKT* west_out, NOC_PKT* east_out, NOC_PKT* north_out, NOC_PKT* south_out,
             bool* west_out_vld, bool* east_out_vld, bool* north_out_vld, bool* south_out_vld);
