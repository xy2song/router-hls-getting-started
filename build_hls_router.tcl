open_project router
set_top router
add_files router.cpp
add_files -tb router_tb.cpp
open_solution "solution0"
set_part {xc7z020clg400-1}

#config_schedule -effort high -verbose
#config_bind -effort high
create_clock -period 3 -name default

csim_design
csynth_design
cosim_design -rtl verilog -trace_level all
#export_design -flow impl -rtl verilog
close_project

exit

