#include "/opt/Xilinx/Vivado/2018.3/include/gmp.h"  //cosim workaround for know Vivado HLS bug
#include "ap_int.h"
#include <stdio.h>
#include "top.h"

void router (ap_uint<NUM_BITS_ROUTER_ADDR> router_addr,
             NOC_PKT left_in, NOC_PKT up_in, NOC_PKT down_in, 
             bool left_in_vld, bool up_in_vld, bool down_in_vld, 
             ap_uint<1>* east_prev_win, ap_uint<1>* north_prev_win, ap_uint<1>* south_prev_win,
             NOC_PKT* west_out, NOC_PKT* east_out, NOC_PKT* north_out, NOC_PKT* south_out, 
             bool* west_out_vld, bool* east_out_vld, bool* north_out_vld, bool* south_out_vld) {

    // relative directions (up, down, left) refer to input ports.
    // absolute directions (north, south, east) refer to output ports.

    ap_uint<2> left_req = NO_REQ;
    ap_uint<2> up_req = NO_REQ;
    ap_uint<2> down_req = NO_REQ;

    bool down_wins_north = 0;
    bool up_wins_north = 0;
    bool down_wins_south = 0;
    bool up_wins_south = 0;
    bool down_deflected_north = 0;
    bool up_deflected_north = 0;
    bool down_deflected_south = 0;
    bool up_deflected_south = 0;

    *west_out_vld = 0;
    *east_out_vld = 0;
    *north_out_vld = 0;
    *south_out_vld = 0;

    ap_uint<NUM_BITS_ROUTER_ADDR> left_in_pkt_dest_addr = left_in.dst_router_addr;
    ap_uint<NUM_BITS_ROUTER_ADDR> up_in_pkt_dest_addr   = up_in.dst_router_addr;
    ap_uint<NUM_BITS_ROUTER_ADDR> down_in_pkt_dest_addr = down_in.dst_router_addr;

    //left input
    if (left_in_vld == 1) { 
        if (left_in_pkt_dest_addr > router_addr){ 
            left_req = SOUTH; // input packet requesting south
        } 
        else if (left_in_pkt_dest_addr < router_addr){ 
            left_req = NORTH;
        }
        else {
            left_req = EAST;
        }
    } 
    if (up_in_vld == 1) { 
        if (up_in_pkt_dest_addr > router_addr){
            up_req = SOUTH;
        } 
        else if (up_in_pkt_dest_addr < router_addr){
            up_req = NORTH;
        } 
        else {
            up_req = EAST;
        }
    }
    if (down_in_vld == 1) { 
        if (down_in_pkt_dest_addr > router_addr){
            down_req = SOUTH;
        } 
        else if (down_in_pkt_dest_addr < router_addr){
            down_req = NORTH;
        }
        else {
            down_req = EAST;
        }
    }

    // arbitration scheme: Left input has least priority. Up and Down inputs round robin.
    down_wins_north      = ( down_req == NORTH && (up_req != NORTH || (up_req == NORTH && *north_prev_win == UP)) ); // down requests north and wins
    up_wins_north        = ( up_req == NORTH && (down_req != NORTH || (down_req == NORTH && *north_prev_win == DOWN)) ); // up requests north and wins
    down_wins_south      = ( down_req == SOUTH && (up_req != SOUTH || (up_req == SOUTH && *south_prev_win == UP)) );
    up_wins_south        = ( up_req == SOUTH && (down_req != SOUTH || (down_req == SOUTH && *south_prev_win == DOWN)) );
    
    down_deflected_north = ( down_req == SOUTH && (up_req == SOUTH && *south_prev_win == DOWN) ); // down requests south but deflected north. (down requesting east will be deflected south if loses)
    up_deflected_north   = ( up_req == SOUTH && (down_req == SOUTH && *south_prev_win == UP)) || ( up_req == EAST  && (down_req == EAST  && *east_prev_win  == UP));
    down_deflected_south = ( down_req == NORTH && (up_req == NORTH && *north_prev_win == DOWN) ) || ( down_req == EAST  && (up_req == EAST  && *east_prev_win  == DOWN) );
    up_deflected_south   = ( up_req == NORTH && (down_req == NORTH && *north_prev_win == UP));

    // output ports
    NORTH_SWITCH: 
    if ( down_wins_north || down_deflected_north ){ 
        *north_out = down_in;  // route down packet to north
    }
    else if ( up_wins_north || up_deflected_north ){
        *north_out = up_in;
    }
    else { 
        *north_out = left_in;
    }

    // if no packets from up and down are exiting north AND left input is requesting north
    if ( down_wins_north || down_deflected_north || up_wins_north || up_deflected_north //let this logic expression be A
    || ( left_req == NORTH ) ) { // let this be B. What we really want is A + A'B, which is equal to A + B
        *north_out_vld = 1;
    }

    SOUTH_SWITCH: 
    if ( down_wins_south || down_deflected_south ){
        *south_out = down_in;
    }
    else if ( up_wins_south || up_deflected_south ){
        *south_out = up_in;
    }
    else { 
        *south_out = left_in;
    }

    if ( down_wins_south || down_deflected_south || up_wins_south || up_deflected_south
    || ( left_req == SOUTH ) ) {
        *south_out_vld = 1;
    }

    WEST_SWITCH: 
    if ( ( left_req == EAST  && (up_req == EAST  || down_req == EAST)) 
      || ( left_req == NORTH && (down_wins_north || down_deflected_north || up_wins_north || up_deflected_north) )
      || ( left_req == SOUTH && (down_wins_south || down_deflected_south || up_wins_south || up_deflected_south) ) )
    { 
        *west_out = left_in;
        *west_out_vld  = 1;
    }
        
    EAST_SWITCH: 
    if ( down_req == EAST && (up_req != EAST || (up_req == EAST && *east_prev_win == UP)) ) // down requests east and wins. packets cannot be deflected east
    { 
        *east_out = down_in;
        *east_prev_win = DOWN;
    }
    else if ( up_req == EAST && (down_req != EAST || (down_req == EAST && *east_prev_win == DOWN)) ) 
    { 
        *east_out = up_in;
        *east_prev_win = UP;
    }
    else { 
        *east_out = left_in;
    }

    if ( ( down_req == EAST && (up_req != EAST || (up_req == EAST && *east_prev_win == UP)) ) 
      || ( up_req == EAST && (down_req != EAST || (down_req == EAST && *east_prev_win == DOWN)) )  
      || ( left_req == EAST && up_req != EAST && down_req != EAST) ) 
    {
        *east_out_vld = 1;
    }

    //update prev_win[router_addr] based on who won
    if (down_wins_north) { 
        *north_prev_win = DOWN;
    }
    else if (up_wins_north) {
        *north_prev_win = UP;
    }

    if (down_wins_south) {
        *south_prev_win = DOWN;
    }
    else if (up_wins_south) {
        *south_prev_win = UP;
    }
    
}


